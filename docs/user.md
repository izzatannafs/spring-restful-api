# User API Spec

## Register User

Endpoint : POST / api/users

Request Body :

```json
{
  "username" : "anaf",
  "password" : "rahasia",
  "name" : "An nafs"
}
```

Response Body (Success) :

```json
{
  "data" : "OK"
}
```

Response Body (Failed) :
```json
{
  "errors" : "User name must not blank,.."
}
```

## Login User

Endpoint : POST / api/auth/login

Request Body :

```json
{
  "username" : "anaf",
  "password" : "rahasia"
}
```

Response Body (Success) :

```json
{
  "data" : {
    "token" : "TOKEN",
    "expiredAt" : 213224444 // milisecond
  }
}
```

Response Body (Failed, 401) :
```json
{
  "errors" : "Username or Password wrong"
}
```
## Get User

Endpoint : GET / api/users/current

Request Header :

- X-API-TOKEN : Token (Mandatory)

Response Body (Success) :

```json
{
  "data" : {
    "username" : "anaf",
    "name" : "An nafs"
  }
}
```

Response Body (Failed, 401) :
```json
{
  "errors" : "Unauthorized"
}
```

## Update User

Endpoint : PATCH / api/users/current

Request Header :

- X-API-TOKEN : Token (Mandatory)

Request Body:
```json
{
  "name" : "An nafs", // put if only want to update name
  "password" : "new password" // put if only want to update password
}
```

Response Body (Success) :

```json
{
  "data" : {
    "username" : "anaf",
    "name" : "An nafs"
  }
}
```

Response Body (Failed, 401) :
```json
{
  "errors" : "Unauthorized"
}
```


## Logout User

Endpoint : DELETE / api/users/logout

Request Header :

- X-API-TOKEN : Token (Mandatory)

Response Body (Success) :

```json
{
  "data" : "OK"
}
```