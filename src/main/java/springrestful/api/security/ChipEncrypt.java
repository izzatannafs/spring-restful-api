package springrestful.api.security;

import org.springframework.stereotype.Component;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

@Component
public class ChipEncrypt {


    private static final String ALGORITHM = "AES";
    private static final String TRANSFORMATION = "AES/CBC/PKCS5Padding";

    private SecretKeySpec getKeySpec(byte[] keyBytes) throws NoSuchAlgorithmException {
        MessageDigest sha = MessageDigest.getInstance("SHA-256");
        byte[] key = sha.digest(keyBytes);
        return new SecretKeySpec(key, ALGORITHM);
    }

    public String encryptMessage(byte[] message, byte[] keyBytes, byte[] ivBytes) throws Exception {
        Cipher cipher = Cipher.getInstance(TRANSFORMATION);
        SecretKeySpec secretKeySpec = getKeySpec(keyBytes);
        IvParameterSpec ivParameterSpec = new IvParameterSpec(ivBytes);
        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivParameterSpec);
        byte[] encryptedMessage = cipher.doFinal(message);
        return Base64.getEncoder().encodeToString(encryptedMessage);
    }

    public String decryptMessage(String encryptedMessage, byte[] keyBytes, byte[] ivBytes) throws Exception {
        Cipher cipher = Cipher.getInstance(TRANSFORMATION);
        SecretKeySpec secretKeySpec = getKeySpec(keyBytes);
        IvParameterSpec ivParameterSpec = new IvParameterSpec(ivBytes);
        cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, ivParameterSpec);
        byte[] decodedMessage = Base64.getDecoder().decode(encryptedMessage);
        byte[] clearMessage = cipher.doFinal(decodedMessage);
        return new String(clearMessage, StandardCharsets.UTF_8);
    }
}




//public class ChipEncrypt {
//
//        private static final String ALGORITMA = "AES";
//        private static final String TRANSFORMATION = "AES/ECB/PKCS5Padding";
//
//        public String encryptMessage(byte[] message, byte[] keyBytes) throws InvalidKeyException, NoSuchPaddingException,
//                NoSuchAlgorithmException, BadPaddingException, IllegalBlockSizeException {
//            Cipher cipher = Cipher.getInstance(TRANSFORMATION);
//            SecretKey secretKey = new SecretKeySpec(keyBytes, ALGORITMA);
//            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
//            byte[] encryptedMessage = cipher.doFinal(message);
//            return Base64.getEncoder().encodeToString(encryptedMessage);
//        }
//
//        public String decryptMessage(String encryptedMessage, byte[] keyBytes) throws NoSuchPaddingException,
//                NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
//            Cipher cipher = Cipher.getInstance(TRANSFORMATION);
//            SecretKey secretKey = new SecretKeySpec(keyBytes, ALGORITMA);
//            cipher.init(Cipher.DECRYPT_MODE, secretKey);
//            byte[] decodedMessage = Base64.getDecoder().decode(encryptedMessage);
//            byte[] clearMessage = cipher.doFinal(decodedMessage);
//            return new String(clearMessage);
//        }
//
//    public static void main(String[] args) throws InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException,
//            BadPaddingException, IllegalBlockSizeException {
//        String encKeyString = "1234567890123456"; // 16-byte key for AES-128
//        String message = "Izzat An nafs";
//
//        ChipEncrypt chipEncrypt = new ChipEncrypt();
//        String encryptedStr = chipEncrypt.encryptMessage(message.getBytes(), encKeyString.getBytes());
//        String decryptedStr = chipEncrypt.decryptMessage(encryptedStr, encKeyString.getBytes());
//
//        System.out.println("Original String: " + message);
//        System.out.println("Encrypted String: " + encryptedStr);
//        System.out.println("Decrypted String: " + decryptedStr);
//    }
//
//}
