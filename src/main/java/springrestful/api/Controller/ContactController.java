package springrestful.api.Controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import springrestful.api.entity.User;
import springrestful.api.model.*;
import springrestful.api.repository.UserRepository;
import springrestful.api.service.ContactService;

import java.util.List;

@RestController
public class ContactController {

//    @Autowired
//    private ContactService contactService;
//
//    @PostMapping(
//            path = "/api/contacts",
//            consumes = MediaType.APPLICATION_JSON_VALUE,
//            produces = MediaType.APPLICATION_JSON_VALUE
//    )
//    public WebResponse<ContactResponse> create(User user, @RequestBody CreateContactRequest request) {
//        ContactResponse contactResponnse = contactService.create(user, request);
//        return WebResponse.<ContactResponse>builder().data(contactResponnse).build();
//    }
//
//    @GetMapping(
//            path = "/api/contacts/{contactId}",
//            produces = MediaType.APPLICATION_JSON_VALUE
//    )
//    public WebResponse<ContactResponse> get(User user, @PathVariable("contactId") String contactId) {
//        ContactResponse contactResponnse = contactService.get(user, contactId);
//        return WebResponse.<ContactResponse>builder().data(contactResponnse).build();
//    }
//
//    @PutMapping(
//            path = "/api/contacts/{contactID}",
//            consumes = MediaType.APPLICATION_JSON_VALUE,
//            produces = MediaType.APPLICATION_JSON_VALUE
//    )
//    public WebResponse<ContactResponse> update(User user,
//                                               @RequestBody UpdateContactRequest request,
//                                               @PathVariable("contactID") String contactID) {
//        request.setId(contactID);
//
//        ContactResponse contactResponnse = contactService.update(user, request);
//        return WebResponse.<ContactResponse>builder().data(contactResponnse).build();
//    }
//
//    @DeleteMapping(
//            path = "/api/contacts/{contactId}",
//            produces = MediaType.APPLICATION_JSON_VALUE
//    )
//    public WebResponse<String> delete(User user,@PathVariable("contactId") String contactId) {
//        contactService.delete(user, contactId);
//        return WebResponse.<String>builder().data("OK").build();
//    }
//
//    @GetMapping(
//            path = "/api/contacts",
//            produces = MediaType.APPLICATION_JSON_VALUE
//    )
//    public WebResponse<List<ContactResponse>> search(User user,
//                                                     @RequestParam(value = "name", required = false) String name,
//                                                     @RequestParam(value = "email", required = false) String email,
//                                                     @RequestParam(value = "phone", required = false) String phone,
//                                                     @RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
//                                                     @RequestParam(value = "size", required = false, defaultValue = "10") Integer size){
//        SearchContactRequest request = SearchContactRequest.builder()
//                .page(page)
//                .size(size)
//                .name(name)
//                .email(email)
//                .phone(phone)
//                .build();
//
//        Page<ContactResponse> contactResponses = contactService.search(user, request);
//        return WebResponse.<List<ContactResponse>>builder()
//                .data(contactResponses.getContent())
//                .paging(PagingResponse.builder()
//                        .currentPage(contactResponses.getNumber())
//                        .totalPage(contactResponses.getTotalPages())
//                        .size(contactResponses.getSize())
//                        .build())
//                .build();
//    }

    @Autowired
    private ContactService contactService;

    @Autowired
    private UserRepository userRepository;

    private static final Logger logger = LoggerFactory.getLogger(ContactController.class);

    @PostMapping(
            path = "/api/contacts",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public WebResponse<ContactResponse> create(User user, @RequestBody CreateContactRequest request) {
        logger.info("Create Contact Request: {}", request);
        ContactResponse contactResponse = contactService.create(user, request);
        return WebResponse.<ContactResponse>builder().data(contactResponse).build();
    }

    @GetMapping(
            path = "/api/contacts/{contactId}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public WebResponse<ContactResponse> get(User user, @PathVariable("contactId") String contactId) {
        logger.info("Get Contact Request: user={}, contactId={}", user, contactId);
        ContactResponse contactResponse = contactService.get(user, contactId);
        return WebResponse.<ContactResponse>builder().data(contactResponse).build();
    }

    @PutMapping(
            path = "/api/contacts/{contactID}",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public WebResponse<ContactResponse> update(User user,
                                               @RequestBody UpdateContactRequest request,
                                               @PathVariable("contactID") String contactID) {
        logger.info("Update Contact Request: user={}, request={}, contactID={}", user, request, contactID);
        request.setId(contactID);

        ContactResponse contactResponse = contactService.update(user, request);
        return WebResponse.<ContactResponse>builder().data(contactResponse).build();
    }

    @DeleteMapping(
            path = "/api/contacts/{contactId}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public WebResponse<String> delete(User user,@PathVariable("contactId") String contactId) {
        logger.info("Delete Contact Request: user={}, contactId={}", user, contactId);
        contactService.delete(user, contactId);
        return WebResponse.<String>builder().data("OK").build();
    }

    @GetMapping(
            path = "/api/contacts",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public WebResponse<List<ContactResponse>> search(User user,
                                                     @RequestParam(value = "name", required = false) String name,
                                                     @RequestParam(value = "email", required = false) String email,
                                                     @RequestParam(value = "phone", required = false) String phone,
                                                     @RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
                                                     @RequestParam(value = "size", required = false, defaultValue = "10") Integer size) {
        logger.info("Search Contacts Request: user={}, name={}, email={}, phone={}, page={}, size={}",
                user, name, email, phone, page, size);
        SearchContactRequest request = SearchContactRequest.builder()
                .page(page)
                .size(size)
                .name(name)
                .email(email)
                .phone(phone)
                .build();

        Page<ContactResponse> contactResponses = contactService.search(user, request);
        return WebResponse.<List<ContactResponse>>builder()
                .data(contactResponses.getContent())
                .paging(PagingResponse.builder()
                        .currentPage(contactResponses.getNumber())
                        .totalPage(contactResponses.getTotalPages())
                        .size(contactResponses.getSize())
                        .build())
                .build();
    }
}
