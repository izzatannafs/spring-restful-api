package springrestful.api.Controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import springrestful.api.entity.Contact;
import springrestful.api.entity.User;
import springrestful.api.model.ContactResponse;
import springrestful.api.model.CreateContactRequest;
import springrestful.api.model.UpdateContactRequest;
import springrestful.api.model.WebResponse;
import springrestful.api.repository.ContactRepository;
import springrestful.api.repository.UserRepository;
import springrestful.api.security.BCrypt;
import springrestful.api.security.ChipEncrypt;

import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class ContactControllerTest {

    @Autowired
    private ChipEncrypt chipEncrypt;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ContactRepository contactRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        contactRepository.deleteAll();
        userRepository.deleteAll();

        User user = new User();
        user.setUsername("test");
        user.setPassword(BCrypt.hashpw("test", BCrypt.gensalt()));
        user.setName("Test");
        user.setToken("test");
        user.setTokenExpiredAt(System.currentTimeMillis() + 10000000L);
        userRepository.save(user);
    }

    @Test
    void createContactBadRequest() throws Exception {
        CreateContactRequest request = new CreateContactRequest();
        request.setFirstName("");
        request.setEmail("salah");

        mockMvc.perform(
                post("/api/contacts")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(request))
                        .header("X-API-TOKEN", "test")
        ).andExpectAll(
                status().isBadRequest()
        ).andDo(result -> {
           WebResponse<String> response = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<WebResponse<String>>() {
            });
           assertNotNull(response.getErrors());
        });
    }

    @Test
    void createContactSuccess() throws Exception {
        CreateContactRequest request = new CreateContactRequest();
        request.setFirstName("Anaf");
        request.setLastName("Nafs");
        request.setEmail("anaf@gmail.com");
        request.setPhone("089606891270");

        mockMvc.perform(
                post("/api/contacts")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(request))
                        .header("X-API-TOKEN", "test")
        ).andExpectAll(
                status().isOk()
        ).andDo(result -> {
            WebResponse<ContactResponse> response = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });
            assertNull(response.getErrors());
            assertEquals("Anaf", response.getData().getFirstName());
            assertEquals("Nafs", response.getData().getLastName());
            assertEquals("anaf@gmail.com", response.getData().getEmail());
            assertEquals("089606891270", response.getData().getPhone());

            assertTrue(contactRepository.existsById(response.getData().getId()));
        });
    }

    @Test
    void getContactNotFound() throws Exception {
        mockMvc.perform(
                get("/api/contacts/23124124")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .header("X-API-TOKEN", "test")
        ).andExpectAll(
                status().isNotFound()
        ).andDo(result -> {
            WebResponse<String> response = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<WebResponse<String>>() {
            });
            assertNotNull(response.getErrors());
        });
    }

    @Test
    void getContactSuccess() throws Exception {
        User user = userRepository.findById("test").orElseThrow();

        Contact contact = new Contact();
        contact.setId(UUID.randomUUID().toString());
        contact.setUser(user);
        contact.setFirstName("Anaf");
        contact.setLastName("Nafs");
        contact.setEmail("anaf@gmail.com");
        contact.setPhone("089606891270");
        contactRepository.save(contact);


        mockMvc.perform(
                get("/api/contacts/" + contact.getId())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .header("X-API-TOKEN", "test")
        ).andExpectAll(
                status().isOk()
        ).andDo(result -> {
            WebResponse<ContactResponse> response = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });
            assertNull(response.getErrors());
            assertEquals(contact.getId(), response.getData().getId());
            assertEquals(contact.getFirstName(), response.getData().getFirstName());
            assertEquals(contact.getLastName(), response.getData().getLastName());
            assertEquals(contact.getEmail(), response.getData().getEmail());
            assertEquals(contact.getPhone(), response.getData().getPhone());
        });
    }

    @Test
    void updateContactBadRequest() throws Exception {
        UpdateContactRequest request = new UpdateContactRequest();
        request.setFirstName("");
        request.setEmail("salah");

        mockMvc.perform(
                put("/api/contacts/12344")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(request))
                        .header("X-API-TOKEN", "test")
        ).andExpectAll(
                status().isBadRequest()
        ).andDo(result -> {
            WebResponse<String> response = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<WebResponse<String>>() {
            });
            assertNotNull(response.getErrors());
        });
    }

    @Test
    void updateContactSuccess() throws Exception {
        User user = userRepository.findById("test").orElseThrow();

        Contact contact = new Contact();
        contact.setId(UUID.randomUUID().toString());
        contact.setUser(user);
        contact.setFirstName("Anaf");
        contact.setLastName("Nafs");
        contact.setEmail("anaf@gmail.com");
        contact.setPhone("089606891270");
        contactRepository.save(contact);


        UpdateContactRequest request = new UpdateContactRequest();
        request.setFirstName("Elfathan");
        request.setLastName("Daniyal");
        request.setEmail("elfathan@gmail.com");
        request.setPhone("089606891275");

        mockMvc.perform(
                put("/api/contacts/" + contact.getId())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(request))
                        .header("X-API-TOKEN", "test")
        ).andExpectAll(
                status().isOk()
        ).andDo(result -> {
            WebResponse<ContactResponse> response = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });
            assertNull(response.getErrors());
            assertEquals(request.getFirstName(), response.getData().getFirstName());
            assertEquals(request.getLastName(), response.getData().getLastName());
            assertEquals(request.getEmail(), response.getData().getEmail());
            assertEquals(request.getPhone(), response.getData().getPhone());

            assertTrue(contactRepository.existsById(response.getData().getId()));
        });
    }

    @Test
    void deleteContactNotFound() throws Exception {
        mockMvc.perform(
                delete("/api/contacts/23124124")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .header("X-API-TOKEN", "test")
        ).andExpectAll(
                status().isNotFound()
        ).andDo(result -> {
            WebResponse<String> response = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<WebResponse<String>>() {
            });
            assertNotNull(response.getErrors());
        });
    }

    @Test
    void deleteContactSuccess() throws Exception {
        User user = userRepository.findById("test").orElseThrow();

        Contact contact = new Contact();
        contact.setId(UUID.randomUUID().toString());
        contact.setUser(user);
        contact.setFirstName("Anaf");
        contact.setLastName("Nafs");
        contact.setEmail("anaf@gmail.com");
        contact.setPhone("089606891270");
        contactRepository.save(contact);


        mockMvc.perform(
                delete("/api/contacts/" + contact.getId())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .header("X-API-TOKEN", "test")
        ).andExpectAll(
                status().isOk()
        ).andDo(result -> {
            WebResponse<String> response = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });
            assertNull(response.getErrors());
            assertEquals("OK", response.getData());
        });
    }

    @Test
    void searchNotFound() throws Exception {
        mockMvc.perform(
                get("/api/contacts")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .header("X-API-TOKEN", "test")
        ).andExpectAll(
                status().isOk()
        ).andDo(result -> {
            WebResponse<List<ContactResponse>> response = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });
            assertNull(response.getErrors());
            assertEquals(0, response.getData().size());
            assertEquals(0, response.getPaging().getTotalPage());
            assertEquals(0, response.getPaging().getCurrentPage());
            assertEquals(10, response.getPaging().getSize());
        });
    }

    @Test
    void searchUsingSuccess() throws Exception {
        User user = userRepository.findById("test").orElseThrow();

        for (int i = 0; i < 100; i++) {
            Contact contact = new Contact();
            contact.setId(UUID.randomUUID().toString());
            contact.setUser(user);
            contact.setFirstName("Anaf " + i);
            contact.setLastName("Nafs");
            contact.setEmail("anaf@gmail.com");
            contact.setPhone("089606891270" + i);
            contactRepository.save(contact);
        }


        mockMvc.perform(
                get("/api/contacts")
                        .queryParam("name", "Ana")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .header("X-API-TOKEN", "test")
        ).andExpectAll(
                status().isOk()
        ).andDo(result -> {
            WebResponse<List<ContactResponse>> response = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });
            assertNull(response.getErrors());
            assertEquals(10, response.getData().size());
            assertEquals(10, response.getPaging().getTotalPage());
            assertEquals(0, response.getPaging().getCurrentPage());
            assertEquals(10, response.getPaging().getSize());
        });

        mockMvc.perform(
                get("/api/contacts")
                        .queryParam("name", "Nafs")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .header("X-API-TOKEN", "test")
        ).andExpectAll(
                status().isOk()
        ).andDo(result -> {
            WebResponse<List<ContactResponse>> response = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });
            assertNull(response.getErrors());
            assertEquals(10, response.getData().size());
            assertEquals(10, response.getPaging().getTotalPage());
            assertEquals(0, response.getPaging().getCurrentPage());
            assertEquals(10, response.getPaging().getSize());
        });

        mockMvc.perform(
                get("/api/contacts")
                        .queryParam("email", "gmail.com")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .header("X-API-TOKEN", "test")
        ).andExpectAll(
                status().isOk()
        ).andDo(result -> {
            WebResponse<List<ContactResponse>> response = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });
            assertNull(response.getErrors());
            assertEquals(10, response.getData().size());
            assertEquals(10, response.getPaging().getTotalPage());
            assertEquals(0, response.getPaging().getCurrentPage());
            assertEquals(10, response.getPaging().getSize());
        });

        mockMvc.perform(
                get("/api/contacts")
                        .queryParam("phone", "1270")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .header("X-API-TOKEN", "test")
        ).andExpectAll(
                status().isOk()
        ).andDo(result -> {
            WebResponse<List<ContactResponse>> response = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });
            assertNull(response.getErrors());
            assertEquals(10, response.getData().size());
            assertEquals(10, response.getPaging().getTotalPage());
            assertEquals(0, response.getPaging().getCurrentPage());
            assertEquals(10, response.getPaging().getSize());
        });

    }

    @Test
    void getContactSuccessAes() throws Exception {
        User user = userRepository.findById("test").orElseThrow();

        // Kunci AES dan IV
        byte[] keyBytes = user.getToken().getBytes(StandardCharsets.UTF_8);
        byte[] ivBytes = "abcdefghijklmnop".getBytes(StandardCharsets.UTF_8);

        Contact contact = new Contact();
        contact.setId(UUID.randomUUID().toString());
        contact.setUser(user);
        // Enkripsi email dan telepon saat menyimpan ke database
        contact.setFirstName("Anaf");
        contact.setLastName("Nafs");
        contact.setEmail(chipEncrypt.encryptMessage("anaf@gmail.com".getBytes(StandardCharsets.UTF_8), keyBytes, ivBytes));
        contact.setPhone(chipEncrypt.encryptMessage("089606891270".getBytes(StandardCharsets.UTF_8), keyBytes, ivBytes));
        contactRepository.save(contact);

        mockMvc.perform(
                get("/api/contacts/" + contact.getId())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .header("X-API-TOKEN", "test")
        ).andExpectAll(
                status().isOk()
        ).andDo(result -> {
            WebResponse<ContactResponse> response = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });
            assertNull(response.getErrors());

            // Dekripsi email dan telepon setelah menerima respons
            String decryptedEmail = chipEncrypt.decryptMessage(response.getData().getEmail(), keyBytes, ivBytes);
            String decryptedPhone = chipEncrypt.decryptMessage(response.getData().getPhone(), keyBytes, ivBytes);

            System.out.println("\nData Sebelum Dekripsi:");
            System.out.println("Email: " + response.getData().getEmail());
            System.out.println("Phone: " + response.getData().getPhone());

            System.out.println("\nData Setelah Dekripsi:");
            System.out.println("Email: " + decryptedEmail);
            System.out.println("Phone: " + decryptedPhone);

            assertEquals(contact.getId(), response.getData().getId());
            assertEquals(contact.getFirstName(), response.getData().getFirstName());
            assertEquals(contact.getLastName(), response.getData().getLastName());
            assertEquals("anaf@gmail.com", decryptedEmail);
            assertEquals("089606891270", decryptedPhone);
        });
    }

//    @Test
//    void updateContactSuccessAes() throws Exception {
//        User user = userRepository.findById("test").orElseThrow();
//
//        // Kunci AES dan IV
//        byte[] keyBytes = user.getToken().getBytes(StandardCharsets.UTF_8);
//        byte[] ivBytes = "abcdefghijklmnop".getBytes(StandardCharsets.UTF_8);
//
//        // Data asli
//        String originalEmail = "anaf@example.com";
//        String originalPhone = "089606891270";
//
//        // Encrypt email and phone before saving to the database
//        String encryptedEmail = chipEncrypt.encryptMessage(originalEmail.getBytes(StandardCharsets.UTF_8), keyBytes, ivBytes);
//        String encryptedPhone = chipEncrypt.encryptMessage(originalPhone.getBytes(StandardCharsets.UTF_8), keyBytes, ivBytes);
//
//        System.out.println("Data Asli:");
//        System.out.println("Email: " + originalEmail);
//        System.out.println("Phone: " + originalPhone);
//
//        System.out.println("\nData Setelah Enkripsi:");
//        System.out.println("Email: " + encryptedEmail);
//        System.out.println("Phone: " + encryptedPhone);
//
//        Contact contact = new Contact();
//        contact.setId(UUID.randomUUID().toString());
//        contact.setUser(user);
//        contact.setFirstName("Anaf");
//        contact.setLastName("Nafs");
//        contact.setEmail(encryptedEmail);
//        contact.setPhone(encryptedPhone);
//        contactRepository.save(contact);
//
//        UpdateContactRequest request = new UpdateContactRequest();
//        request.setId(contact.getId());
//        request.setFirstName("Elfathan");
//        request.setLastName("Daniyal");
//        // Encrypt new email and phone before updating
//        String newEncryptedEmail = chipEncrypt.encryptMessage("elfathan@gmail.com".getBytes(StandardCharsets.UTF_8), keyBytes, ivBytes);
//        String newEncryptedPhone = chipEncrypt.encryptMessage("089606891275".getBytes(StandardCharsets.UTF_8), keyBytes, ivBytes);
//        request.setEmail(newEncryptedEmail);
//        request.setPhone(newEncryptedPhone);
//
//        System.out.println("\nRequest Data Setelah Enkripsi:");
//        System.out.println("Email: " + newEncryptedEmail);
//        System.out.println("Phone: " + newEncryptedPhone);
//
//        mockMvc.perform(
//                put("/api/contacts/" + contact.getId())
//                        .accept(MediaType.APPLICATION_JSON)
//                        .contentType(MediaType.APPLICATION_JSON_VALUE)
//                        .content(objectMapper.writeValueAsString(request))
//                        .header("X-API-TOKEN", "test")
//        ).andExpectAll(
//                status().isOk()
//        ).andDo(result -> {
//            WebResponse<ContactResponse> response = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
//            });
//            assertNull(response.getErrors());
//
//            // Decrypt email and phone after receiving the response
//            String decryptedEmail = chipEncrypt.decryptMessage(response.getData().getEmail(), keyBytes, ivBytes);
//            String decryptedPhone = chipEncrypt.decryptMessage(response.getData().getPhone(), keyBytes, ivBytes);
//
//            System.out.println("\nData Setelah Dekripsi:");
//            System.out.println("Email: " + decryptedEmail);
//            System.out.println("Phone: " + decryptedPhone);
//
//            assertEquals("Elfathan", response.getData().getFirstName());
//            assertEquals("Daniyal", response.getData().getLastName());
//            assertEquals("elfathan@gmail.com", decryptedEmail);
//            assertEquals("089606891275", decryptedPhone);
//
//            assertTrue(contactRepository.existsById(response.getData().getId()));
//        });
//    }

}